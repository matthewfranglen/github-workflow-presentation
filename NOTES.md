### High Level Overview

The rough outline will be:

 * Outline of talk
 * Process does not set you free (Brazil)
 * What do you want to achieve
 * How to do that (hub command, forking, PRs)
 * Looking at PRs locally
 * Valuable feedback on PRs (Jack "Matt PR review" gif)

I want to put this on gitlab so I can have the CI build pages and don't have to worry about permissions.

### Outline

I'm going to talk about using github effectively.
Github is more than just a place where you can back up your code.
By integrating the basic parts of github into your workflow you can produce high quality understandable science.

### Process does not set you free

I am an engineer, and engineers use github in a particular way.
The process that engineers use is more rigid than what you likely require.
This is because engineers are concerned with maintenance of code.
For software development the total maintenance cost is between 60-80% (i.e. 150% to 400% of the initial development cost)

The wrong process will make you slower for little benefit.

### What do you want to achieve

The processes that work for engineers are likely to be too onerous for data scientists.
I think that you value the ability to communicate and reproduce your findings, not maintenance.
When reviewing the output of another try to keep this in mind.

### How to promote communication and reproducibility

I can show you the `hub` command which makes it easy to use github.

`brew install hub`

`hub clone BrandwatchLtd/SOME_REPO`

`hub fork`

`git push -u YOUR_GITHUB_USERNAME BRANCH_NAME`

`hub pull-request`

Check out pull requests locally:

https://gist.github.com/piscisaureus/3342247
https://gist.github.com/gnarf/5406589

`git checkout pr/5`

### Valuable PR Feedback

Can you understand why something was done?

Can you understand how something was done?

Remember there is another person at the other end of that PR.
Everyone is proud of their work.
Thoughtful constructive criticism is a chance to grow.

Approve/Comment/Decline - I probably wouldn't bother.
These are more important for engineering where the choice impacts on the stability of a live platform.

https://media.giphy.com/media/Zdwu5CrFfm88g/giphy.gif
